(define (tree-map items operation)
  (map (lambda (item)
         (if (pair? item)
             (tree-map item operation)
             (operation item)))
       items))

(tree-map
 (list 1
       (list 2
             (list 3 4) 5)
       (list 6 7)) (lambda (x) (* x x)))
