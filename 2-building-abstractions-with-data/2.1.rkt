(define (make-rat n d)
  (let ((g (gcd n d)))
   (cond ((and (< n 0) (> d 0)) (cons (/ n g) (/ d g)))
         ((and (> n 0) (< d 0)) (cons (- (/ n g)) (- (/ d g))))
         ((and (< n 0) (< d 0)) (cons (- (/ n g)) (- (/ d g))))
         (else (cons (/ n g) (/ d g))))))


(define (numer rat) (car rat))
(define (denom rat) (cdr rat))

(define (print-rat x)
(newline)
(display (numer x))
(display "/")
(display (denom x)))


(print-rat (make-rat 2 3))
(print-rat (make-rat -1 3))
(print-rat (make-rat 1 -3))
(print-rat (make-rat -1 -3))



(define (better-make-rat n d)
   (let ((g ((if (< d 0) - +) (gcd n d))))
     (cons (/ n g) (/ d g))))


